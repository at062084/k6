# [3.1.0](https://gitlab.com/to-be-continuous/k6/compare/3.0.0...3.1.0) (2022-10-04)


### Features

* normalize reports ([d1a5d6f](https://gitlab.com/to-be-continuous/k6/commit/d1a5d6fb1fb11a329f168f1bcb0a4ae2f06a1a42))

# [3.0.0](https://gitlab.com/to-be-continuous/k6/compare/2.1.0...3.0.0) (2022-08-05)


### Features

* adapative pipeline ([347464c](https://gitlab.com/to-be-continuous/k6/commit/347464cbff782af897d2e049e746e6e19494e9eb))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [2.1.0](https://gitlab.com/to-be-continuous/k6/compare/2.0.1...2.1.0) (2022-05-01)


### Features

* configurable tracking image ([3da1826](https://gitlab.com/to-be-continuous/k6/commit/3da1826da0ae2b4e34bd2af0ac1b19dc6f52d4f1))

## [2.0.1](https://gitlab.com/to-be-continuous/k6/compare/2.0.0...2.0.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([78b3ba5](https://gitlab.com/to-be-continuous/k6/commit/78b3ba5f0b52d835dacebcbb360f813cfef97bed))

## [2.0.0](https://gitlab.com/to-be-continuous/k6/compare/1.0.0...2.0.0) (2021-09-03)

### Features

* Change boolean variable behaviour ([f3112ac](https://gitlab.com/to-be-continuous/k6/commit/f3112ac0ae4e84ce4014a220dd5093e395a3aab2))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## 1.0.0 (2021-07-23)

### Features

* add scoped variables support ([a74e805](https://gitlab.com/to-be-continuous/k6/commit/a74e8054d3dc728f27628ade37af2dceee6884e9))
* initialisation of k6 template ([4a38e3e](https://gitlab.com/to-be-continuous/k6/commit/4a38e3ec6a6d3608f25ef985f17d4b2a81774f82))
